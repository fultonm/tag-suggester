﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

public class TagSuggester
{
    private Dictionary<string, List<Note>> m_items;

    public static void Main()
    {
        TagSuggester suggester = new TagSuggester();      
        int noteCount = int.Parse(Console.ReadLine());
        for (int i = 0; i < noteCount; i++)
        {
            suggester.AddNote(new Note("", Console.ReadLine()));
        }
        int queryCount = int.Parse(Console.ReadLine());
        for (int q = 0; q < queryCount; q++)
        {
            List<string> suggestions = suggester.GetSuggestions(Console.ReadLine());
            string output = suggestions.Count == 0 ? "" : suggestions[0];
            for (int i = 1; i < suggestions.Count; i++)
            {
                output += "," + suggestions[i];
            }
            Console.WriteLine(output);
        }
    }

    public TagSuggester()
    {
        m_items = new Dictionary<string, List<Note>>();
    }

    public void AddNote(Note note)
    {
        foreach (string t in note.GetTags())
        {
            string tag = t.ToLower();
            List<Note> list;
            if (m_items.ContainsKey(tag))
            {
                list = m_items[tag];
            }
            else
            {
                list = new List<Note>();
                m_items.Add(tag, list);
            }
            list.Add(note);
        }
    }

    public List<string> GetSuggestions(string prefix)
    {
        prefix = prefix.ToLower();
        List<Suggestion> suggestions = new List<Suggestion>();
        foreach (string tag in m_items.Keys)
        {
            // the edits variable is for later sorting the list of
            // possible suggestions. Low edits value = high priority suggestion
            int edits;
            if (tag.StartsWith(prefix))
            {
                edits = 0;
            }
            // We pass in a substring of the possible suggestion with a maximum length of the user input
            // Ex. input: cooie, substring: cooki
            else if (EqualsIgnoreTypo(prefix, tag.Substring(0, Math.Min(tag.Length, prefix.Length))))
            {
                edits = 1;
            }
            else
            {
                edits = 2;
            }
            if (edits < 2)
            {
                suggestions.Add(new Suggestion(tag, edits, m_items[tag].Count));
            }
        }
        suggestions.Sort();
        List<string> tags = new List<string>(suggestions.Count);
        foreach (Suggestion s in suggestions)
        {
            tags.Add(s.m_tag);
        }
        return tags;
    }

    /// <summary>
    /// Uses a variety of tests to compare the input with tags.
    /// </summary>
    /// <param name="suspect">The word in question</param>
    /// <param name="word">The word to compare against</param>
    /// <returns>Returns true if the words are a match,
    /// or if any test indicates the words may be a match</returns>
    public static bool EqualsIgnoreTypo(string suspect, string word)
    {
        bool typo = suspect.Equals(word);

        // letter deletion
        // try deleting letter from suspect to see if it equals word
        typo = typo || EqualIfDeleteLetter(suspect, word);

        // letter addition
        // adding to suspect is the same as deleting from word
        typo = typo || EqualIfDeleteLetter(word, suspect);

        // Method returns true if the word is a match if a letter is omitted
        typo = typo || EqualIfForgotLetter(suspect, word);

        // Method returns true if two letters are swapped
        typo = typo || EqualIfSwapLetter(suspect, word);

        // letter substitution
        // count how many letters are different between the two words.
        if (!typo && suspect.Length == word.Length)
        {
            int diff = 0;
            for (int i = 0; i < suspect.Length && diff < 2; i++)
            {
                if (suspect[i] != word[i])
                {
                    diff++;
                }
            }
            typo = diff == 1;
        }
        return typo;
    }

    /// <summary>
    /// Compares two strings to determine if swapping two letters in
    /// the suspect makes them a match.
    /// </summary>
    /// <param name="suspect">The word in question</param>
    /// <param name="word">The known word</param>
    /// <returns>Returns true if the words are a match</returns>
    private static bool EqualIfSwapLetter(string suspect, string word)
    {
        int i = 0;
        while (i < suspect.Length && suspect[i] == word[i])
        {
            i++;
        }
        if (i + 1 < suspect.Length)
        {
            Char swap = suspect[i];
            StringBuilder sbSuspect = new StringBuilder(suspect);
            sbSuspect[i] = suspect[i + 1];
            sbSuspect[i + 1] = swap;
            suspect = sbSuspect.ToString();
        }
        while (i < suspect.Length && suspect[i] == word[i])
        {
            i++;
        }
        return i == suspect.Length;

    }

    /// <summary>
    /// Determines if the user forgot a letter while typing a word.
    /// </summary>
    /// <param name="suspect">String containing the user input.</param>
    /// <param name="word">String containing suggestion to test against.</param>
    /// <returns>Return true if this suggestion is a match.</returns>
    private static bool EqualIfForgotLetter(string suspect, string word)
    {

        int i = 0;
        while (i < suspect.Length && suspect[i] == word[i])
        {
            i++;
        }
        while (i < word.Length - 1 && suspect[i] == word[i + 1])
        {
            i++;
        }
        return i == word.Length - 1;
    }

    /// <summary>
    /// Determines if the inputs are a match if the letter which doesn't match
    /// is omitted.
    /// </summary>
    /// <param name="suspect">The string which may have too many letters</param>
    /// <param name="word">The string to test against</param>
    /// <returns>Returns true if the inputs are match sans 1 char.</returns>
    private static bool EqualIfDeleteLetter(string suspect, string word)
    {
        if (suspect.Length - word.Length != 1)
        {
            return false;
        }
        int i = 0;
        while (i < word.Length && suspect[i] == word[i])
        {
            i++;
        }
        while (i + 1 < suspect.Length && i < word.Length && suspect[i + 1] == word[i])
        {
            i++;
        }
        return i == word.Length;
    }
}

public class Note
{
    private List<string> m_tags;
    private string m_content;

    public Note(string content, string tags)
    {
        List<string> list = new List<string>();
        foreach (string s in tags.Split(new char[] { ',' }))
        {
            list.Add(s);
        }
        m_content = content;
        m_tags = list;
    }

    public string getContent()
    {
        return m_content;
    }

    public ICollection<string> GetTags()
    {
        return new ReadOnlyCollection<string>(m_tags);
    }
}

/// <summary>
/// 
/// </summary>
class Suggestion : IComparable<Suggestion>
{
    // Represents how close of a match this suggestion is.
    // 0 = exact
    // 1 = typo occured, could be a match
    // 2 = not a match
    public readonly int m_edits;
    // Number of notes using this tag
    public readonly int m_count;
    // Tag content
    public readonly string m_tag;

    public Suggestion(string tag, int edits, int count)
    {
        m_tag = tag;
        m_edits = edits;
        m_count = count;
    }

    /// <summary>
    /// Used for sorting tags to be display first.
    /// We choose first the tags that are exactly equal.
    /// Then we choose by how often they appear in notes
    /// Then ordered alphabetically
    /// </summary>
    /// <param name="o">Other suggestion</param>
    /// <returns></returns>
    public int CompareTo(Suggestion o)
    {
        int compare = m_edits - o.m_edits;
        if (compare == 0)
        {
            compare = o.m_count - m_count;
        }
        if (compare == 0)
        {
            compare = o.m_tag.CompareTo(m_tag);
        }
        return compare;
    }
}